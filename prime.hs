
isPrime :: Integral a => a -> Bool
isPrime a = if a == 0 || a == 1
                then False
                else all (\x -> a `mod` x /= 0) [2..a-1]

allPrimes :: Integral a => [(a,a)]
allPrimes = zip [1..] (filter isPrime [0..])

main :: IO ()
main = mapM_ (putStrLn) (take 4242 ( map (\(a,b) -> show a ++ " - " ++ show b) allPrimes))

