#!/bin/zsh
function ghci_launcher()
{
	expect <(echo log_user 0 ; echo spawn ghci ; echo send \":set +t\\n\" ; echo interact)
}

ghci_launcher
