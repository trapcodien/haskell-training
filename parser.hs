import Data.Char
import Control.Monad
import qualified Control.Applicative as CA

data Parser a = Parser { runParser :: (String -> [(a,String)]) }

instance Monad Parser where
        return x = Parser $ \str -> [(x,str)]
        p >>= f = Parser $ \str -> do 
                                      (x,str') <- runParser p str
                                      runParser (f x) str'

instance MonadPlus Parser where
        mzero = Parser $ \str -> []
        p `mplus` q = Parser $ \str -> runParser p str ++ runParser q str



instance CA.Alternative Parser where
        empty = mzero
        (<|>) = mplus

instance Functor Parser where
        fmap = liftM


instance CA.Applicative Parser where
        pure = return
        (<*>) = ap


(+++) :: Parser a -> Parser a -> Parser a
p +++ q = Parser $ \str -> case runParser p str ++ runParser q str of
                               [] -> []
                               (x:xs) -> [x]

getch :: Parser Char
getch = Parser $ \str -> case str of
                             "" -> []
                             (x:xs) -> [(x,xs)]

consume :: Parser ()
consume = getch >> return ()

sat :: (Char -> Bool) -> Parser Char
sat f = do 
           c <- getch
           if f c then return c 
                  else mzero

char :: Char -> Parser Char
char c = sat (==c)

letter :: Parser Char
letter = sat isLetter

string :: String -> Parser String
string "" = return ""
string (x:xs) = char x >> string xs >> return (x:xs) 

many :: Parser a -> Parser [a]
many1 :: Parser a -> Parser [a]
many p = many1 p +++ return []
many1 p = do 
            a <- p
            as <- many p
            return (a:as)

sepby :: Parser a -> Parser b -> Parser [a]
sepby1 :: Parser a -> Parser b -> Parser [a]
p `sepby` sep = (p `sepby1` sep) +++ return []
p `sepby1` sep = do 
                   a <- p
                   as <- many (do {sep ; p})
                   return (a:as)


word :: Parser String
word = word' +++ return ""
    where
        word' = do
            x <- letter
            xs <- word
            return  (x:xs)

space :: Parser String
space = many1 (sat isSpace)

token :: Parser a -> Parser a
token p = p >>= \a -> (many $ sat isSpace) >> return a 

symb :: String -> Parser String
symb str = token (string str)

apply :: Parser a -> String -> [(a, String)]
apply p str = runParser (do many $ sat isSpace ; p) str


