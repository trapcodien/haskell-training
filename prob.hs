import Control.Monad
import Control.Applicative
import Data.Ratio

newtype Prob a = Prob { getProb :: [(a,Rational)] } deriving (Show)

flatten :: Prob (Prob a) -> Prob a
flatten (Prob xss) = Prob $ concat $ map multAll xss where
    multAll ((Prob xs),p) = map (\(a,ratio) -> (a,ratio*p)) xs


instance Functor Prob where
        fmap f (Prob xs) = Prob $ map (\(a,ratio) -> (f a, ratio)) xs

instance Applicative Prob where
        pure x = Prob [(x,1%1)]
        fs <*> xs = flatten $ fmap (\f -> fmap (\x -> f x) xs) fs

instance Monad Prob where
         return = pure
         m >>= f = flatten $ fmap f m
         fail _ = Prob []

--------------------------
data Piece = Pile | Face deriving (Show,Eq)

coin :: Prob Piece
coin = Prob [(Pile,1%2),(Face,1%2)]

cheatedCoin :: Prob Piece
cheatedCoin = Prob [(Pile,1%10),(Face,9%10)]

test :: Prob Bool
test = Prob [getChanges flipThree True, getChanges flipThree False] where
        flipThree = do
            a <- coin
            b <- coin
            c <- cheatedCoin
            return $ any (==Face) [a,b,c]
        getChanges proba bool = (bool, (sum $ map snd $ filter (\(a,b) -> a == bool) (getProb proba)))
--------------------------

