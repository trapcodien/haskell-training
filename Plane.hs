module Plane (toPlane, pget, pset, Plane) where
---------------------------- Plane Module --------------------------------------


import Data.Sequence
import Control.Monad
import Data.Maybe

type Plane a = Seq (Seq a) -- This is horrible, use arrays

------------------------------ Private------------------------------------------
-- sindex is a Secure index
sindex :: Seq a -> Int -> Maybe a
sindex p i
    | i >= Data.Sequence.length p = Nothing
    | i < 0 = Nothing
    | otherwise = Just $ index p i

-- update' jsut flip sequence and elem.
update' :: Int -> Seq a -> a -> Seq a
update' x s e = update x e s

--------------------------------------------------------------------------------

toPlane :: [[a]] -> Plane a
toPlane = fromList . fmap fromList

pget :: Int -> Int -> Plane a -> Maybe a
pget x y p = flip sindex x =<< sindex p y

pset :: Plane a -> Int -> Int -> a -> Maybe (Plane a)
pset p x y e = update' y p <$> update x e <$> sindex p y


