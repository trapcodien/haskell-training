import Control.Monad

readEither :: (Read a) => String -> Either String a
readEither str = f $ reads str
    where f [(a,_)] = Right a
          f _       = Left ("Parse error: " ++ (head $ words str))

foldingFunction :: [Double] -> String -> Either String [Double]
foldingFunction (a:b:xs) "+" = return ((a + b):xs)
foldingFunction (a:b:xs) "-" = return ((a - b):xs)
foldingFunction (a:b:xs) "*" = return ((a * b):xs)
foldingFunction (a:b:xs) "/" = return ((a / b):xs)
foldingFunction (a:b:xs) "^" = return ((a ** b):xs)
foldingFunction xs "sum" = return ([sum xs])
foldingFunction xs "product" = return ([product xs])
foldingFunction (a:xs) "ln" = return ((log a):xs)
foldingFunction xs num = fmap (:xs) (readEither num)

solveRPN :: String -> Either String Double
solveRPN str = do
        r <- foldM foldingFunction [] (words str)
        if r == [] then 
                    Left "Error: Listis empty"
                   else
                    return $ head r

