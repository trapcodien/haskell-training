
data Graph a = Nil | Node a [Graph a] deriving Show

instance Functor Graph where
    fmap f Nil = Nil
    fmap f (Node x xs) = Node (f x) (expand xs) where
        expand = map (\g -> fmap f g)

g = Node 1 [Nil,Nil,Node 2 [Nil],Node 3 [Node 4 [Nil], Node 5 [Node 6 [Nil]]]]


