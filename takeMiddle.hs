
import Data.List

takeMiddle :: [a] -> a
takeMiddle l = l !! (length l `div` 2)

