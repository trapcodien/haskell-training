
comb :: String
comb = unwords [ show a ++ show b ++ show c |
        a <- [0..9],
        b <- [0..9],
        c <- [0..9],
            a < b,
            b < c ]

