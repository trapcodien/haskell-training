import Data.Char
import Data.Maybe

isValid  :: Integer -> Bool
isValid n = n > 0 && check
    where
    check = (n `mod` (addAllDigits $ show n) == 0)
        where
        addAllDigits str = foldl (+) 0 (map (toInteger . digitToInt) str)

getNext :: Integer :: Integer
getNext = until isValid succ . succ

getSerie :: Int -> Maybe Integer -> [Integer]
getSerie n start = take n $ filter isValid [(fromMaybe 0 start) + 1..]
