import Data.Char
import System.Environment

main = do
    args <- getArgs
    prog <- getProgName
    mapM putStrLn args
    putStrLn prog

