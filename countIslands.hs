-- State Monads Example (CountIslands)

import Control.Monad.State
import Data.Char (toLower)
import Data.Foldable (toList)
import Data.Maybe
import Data.Word
import Plane
import qualified Data.ByteString as B

type Zone = Plane Char
data Islands = Islands { getZone :: Zone, getN :: Int }

instance Show Islands where
    show (Islands zone _) = unlines $ toList $ fmap toList zone

getCell x y zone = toLower <$> (pget x y zone)
setCell zone x y n = fromJust $ pset zone x y (head $ show n)

island_increment_n :: State Islands ()
island_increment_n =
    do
        Islands zone n <- get
        put (Islands zone (n + 1))

island_propagate :: Int -> Int -> State Islands ()
island_propagate x y =
    do
        Islands zone n <- get
        if getCell x y zone == Just 'x' then do
                                            put $ Islands (setCell zone x y n) n 
                                            island_propagate (x + 1) (y)
                                            island_propagate (x - 1) (y)
                                            island_propagate (x) (y + 1)
                                            island_propagate (x) (y - 1)
                                         else return ()

count_islands ::State Islands ()
count_islands = ci 0 0
    where
        ci x y = do
            Islands zone n <- get
            if n < 10 then do
            case getCell x y zone of 
                                 Just 'x' -> island_propagate x y >> island_increment_n >> ci x y
                                 Nothing -> return ()
                                 otherwise -> ci (x + 1) (y) >> ci (x) (y + 1)
            else return ()

get_map :: IO Islands
get_map =
    do
        content <- getContents
        return $ Islands (toPlane $ lines content) 0

main :: IO ()
main =
    do
        content <- get_map
        print $ snd $ runState count_islands content
