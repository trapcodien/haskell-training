import Control.Monad
import Control.Applicative

data AST a = Term a | Expr [AST a] deriving Show

instance Functor AST where
        fmap f (Term x) = Term $ f x
        fmap f (Expr xs) = Expr $ fmap (\ast -> fmap f ast) xs

instance Applicative AST where
        pure x = Term x
        Term f <*> Term x = Term $ f x
        Term f <*> xs = fmap f xs
        fs <*> Term x = fmap (\f -> (f x)) fs
        fs <*> xs = join $ fmap (\f -> fmap f xs) fs

instance Monad AST where
        return = pure
        m >>= f = join (fmap f m)
