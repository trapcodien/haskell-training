qsort :: Ord t => [t] -> [t]
qsort [] = []
qsort (x:xs) = qsort less ++ [x] ++ qsort greater
    where
        greater =       [e | e <- xs, e >= x]
        less =          [e | e <- xs, e < x]

