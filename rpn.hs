
-- Simple RPN calculator.

rpn :: (Floating a, Read a) => String -> a
rpn = head . foldl f [] . words where
    f (a:b:xs) "+" = (a + b):xs
    f (a:b:xs) "*" = (a * b):xs
    f (a:b:xs) "-" = (a - b):xs
    f (a:b:xs) "/" = (a / b):xs
    f (a:b:xs) "^" = (a ** b):xs
    f (xs) "sum" = [sum xs]
    f (a:xs) "ln" = log a:xs
    f xs num = read num:xs

